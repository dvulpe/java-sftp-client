package uk.co.dvulpe.sftptest;

public class SftpClientException extends RuntimeException {

    public SftpClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
