package uk.co.dvulpe.sftptest;

import java.util.List;

public interface SftpClient extends AutoCloseable {
    void connect(String host, Integer port, String username, String password, String hostKey);

    List<String> listFilesInPath(String path);

    void downloadFile(String path, String destination);
}
