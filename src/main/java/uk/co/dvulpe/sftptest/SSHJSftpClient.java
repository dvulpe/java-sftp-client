package uk.co.dvulpe.sftptest;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.RemoteResourceInfo;
import net.schmizz.sshj.sftp.SFTPClient;

import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class SSHJSftpClient implements SftpClient {
    private final SSHClient sshClient;
    private SFTPClient sftpClient;

    public SSHJSftpClient() {
        this.sshClient = new SSHClient();
    }

    @Override
    public void connect(String host, Integer port, String username, String password, String hostKey) {
        try {
            sshClient.addHostKeyVerifier(hostKey);
            sshClient.connect(host, port);
            sshClient.authPassword(username, password);
            sftpClient = sshClient.newSFTPClient();
        } catch (IOException e) {
            throw new SftpClientException("Exception during connection", e);
        }
    }

    @Override
    public List<String> listFilesInPath(String path) {
        try {
            return sftpClient.ls(path)
                    .stream()
                    .map(RemoteResourceInfo::getName)
                    .filter(it -> it.endsWith(".txt"))
                    .collect(toList());
        } catch (IOException e) {
            throw new SftpClientException("Exception during list files", e);
        }
    }

    @Override
    public void downloadFile(String path, String destination) {
        try {
            sftpClient.get(path, destination);
        } catch (IOException e) {
            throw new SftpClientException("Exception during download file", e);
        }
    }

    @Override
    public void close() throws Exception {

    }
}
