package uk.co.dvulpe.sftptest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JschToSLF4jLogger implements com.jcraft.jsch.Logger {
    private final Logger LOGGER = LoggerFactory.getLogger(JschToSLF4jLogger.class);


    public JschToSLF4jLogger() {
    }

    @Override
    public boolean isEnabled(int level) {
        return true; // log all
    }

    @Override
    public void log(int level, String message) {
        switch (level) {
            case DEBUG:
                LOGGER.debug(message);
                break;
            case INFO:
                LOGGER.info(message);
                break;
            case FATAL:
                LOGGER.error(message);
                break;
            case ERROR:
                LOGGER.error(message);
                break;
            default:
                break;
        }
    }
}
