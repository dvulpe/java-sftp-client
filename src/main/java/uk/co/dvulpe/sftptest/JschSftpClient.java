package uk.co.dvulpe.sftptest;

import com.jcraft.jsch.*;

import java.util.Base64;
import java.util.List;
import java.util.Vector;

import static java.util.stream.Collectors.toList;

public class JschSftpClient implements SftpClient {

    public static final String SFTP = "sftp";
    private final JSch jsch;
    private Session session;
    private ChannelSftp channelSftp;

    public JschSftpClient() {
        JSch.setLogger(new JschToSLF4jLogger());
        this.jsch = new JSch();
    }

    @Override
    public void connect(String host, Integer port, String username, String password, String hostKey) {
        try {
            this.session = jsch.getSession(username, host, port);
            session.setPassword(password);
            jsch.getHostKeyRepository().add(new HostKey(host, Base64.getDecoder().decode(hostKey)), null);

            session.connect();

            Channel sftp = session.openChannel(SFTP);
            sftp.connect();
            this.channelSftp = (ChannelSftp) sftp;
        } catch (JSchException e) {
            throw new SftpClientException("Exception during connect", e);
        }
    }

    @Override
    public List<String> listFilesInPath(String path) {
        try {
            Vector<ChannelSftp.LsEntry> ls = channelSftp.ls(path);
            return ls.stream().map(ChannelSftp.LsEntry::getFilename)
                    .filter(it -> it.endsWith(".txt"))
                    .collect(toList());
        } catch (SftpException e) {
            throw new SftpClientException("Exception when listing files in folder", e);
        }
    }

    @Override
    public void downloadFile(String path, String destination) {
        try {
            channelSftp.get(path, destination);
        } catch (SftpException e) {
            throw new SftpClientException("Exception when downloading file " + path, e);
        }
    }


    @Override
    public void close() throws Exception {
        if (session != null) {
            this.session.disconnect();
        }
        if (channelSftp != null) {
            this.channelSftp.disconnect();
        }
    }
}
