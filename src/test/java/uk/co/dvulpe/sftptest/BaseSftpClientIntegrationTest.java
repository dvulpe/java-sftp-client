package uk.co.dvulpe.sftptest;

import org.hamcrest.MatcherAssert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static java.nio.file.Files.readAllLines;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public abstract class BaseSftpClientIntegrationTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private final SftpClient underTest;
    private static final String LOCALHOST = "127.0.0.1";
    private static final int PORT = 9200;
    private static final String USERNAME = "service";
    private static final String PASSWORD = "p4ssw0rd";
    private final String hostKey;

    protected BaseSftpClientIntegrationTest(SftpClient underTest, String hostKey) {
        this.underTest = underTest;
        this.hostKey = hostKey;
    }

    @Test
    public void testIntegration() throws IOException {
        underTest.connect(LOCALHOST, PORT, USERNAME, PASSWORD, hostKey);

        MatcherAssert.assertThat(underTest.listFilesInPath("/home/service"), is(singletonList("sample.txt")));

        File temporaryFile = temporaryFolder.newFile();
        underTest.downloadFile("/home/service/sample.txt", temporaryFile.toString());

        assertThat(readAllLines(temporaryFile.toPath()), is(singletonList("SAMPLE FILE")));
    }
}
