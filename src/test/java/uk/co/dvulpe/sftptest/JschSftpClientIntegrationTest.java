package uk.co.dvulpe.sftptest;

public class JschSftpClientIntegrationTest extends BaseSftpClientIntegrationTest {

    public JschSftpClientIntegrationTest() {
        super(new JschSftpClient(), "AAAAB3NzaC1yc2EAAAADAQABAAABAQC9J0coUonZ7AXxDfKC913dSF8brhOdrvYhsdA+OX2gk1FBz6jUZUfktIOyLYiIpGB6Vs/9qb2E5ayEGuNw2FwOnWMRrGynrLkypIBulVCchyc0ujq064JYkulautIZ+XqVCtEfD7PPYS2Ugpvu6TqA0u8j1+ZYxg92IT2G/7cm2COfkJzg0irFJv8puATyKAScUh7IRrKfaYz0ZqiGbXIH1Nrt9If54aDPqGKuEqtlhKcL+bIe69tJFY629rN3qEF6+RnZt2Hl3WCVtJRUHQV/eQog0PZGEgP65ffAfb8YW+684LR0kCayW9YG5HmiaC05XiELG7qMt5f/2ExXGCFb");
    }
}