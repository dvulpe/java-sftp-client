#!/bin/bash

set -euo pipefail

echo "Waiting for SSH server to come up"
until nc localhost 9200 &>/dev/null; do
    sleep 1s;
done
echo "SSH server is up and running"
